---
layout: page
title: Events
permalink: /programme/events/
sponsors: true
---

We have a number of events during the week at linux.conf.au 2020.
If you have any last minute questions email [events@lca2020.linux.org.au](mailto:events@lca2020.linux.org.au) or head to the registration desk.

<a id="woot-breakfast"></a>
## Women of Open Technology Working Breakfast

Tuesday 14 January, 7am

_Requires a ticket. Please check your Dashboard._

#### Vital information

* Meet at the Convention Centre from 7am
* Breakfast will be served from 7am, and the discussion will start shortly after
* You must have a ticket to the breakfast, which you can confirm via your [Dashboard](/dashboard/)
* The WoOT Breakfast should finish around 8:45am

#### How do I get there?

Please go to Room 5 in the Gold Coast Convention and Exhibition Centre.

---

<a id="speakers-dinner"></a>
## Speakers' Dinner

Tuesday 14 January, 7pm

_For Speakers' Dinner ticket holders only._

#### Vital information

* Meet at the Convention Centre from 6:15pm
* Speakers' Dinner starts at 7pm at the Skypoint Observation Deck at Q1 Tower at Surfers Paradise
* Use your travel card to take the light rail to Surfers Paradise (if you do not have a travel card, eg. for a plus one, we will have some spare go cards available to give you)
* You must have either a 1 or 2 next to the Speakers' Dinner icon (the leftmost icon, with the microphone) on your badge to attend
* The Speakers' Dinner should finish around 10pm

#### How do I get there?

At the Broadbeach North station (in front of the convention centre), touch on, and wait on Platform 1 for the next tram to Helensvale.
We will have volunteers at this stop to direct you.

When you get to Surfers Paradise, touch off with your card, and cross the road to the Q1 building.
Again, we will have volunteers here to welcome you.

Enter the Skypoint lobby, and you will be directed up in the lift to Level 78, where you will have your badge checked, and be given a wristband.

#### Additional info

If you are planning to stay in Surfers Paradise after the event, just be mindful that trams finish around 12am, but busses will continue throughout the night.

---

<a id="penguin-dinner"></a>
## Penguin Dinner

Wednesday 15 January, 6:30pm

_For Penguin Dinner ticket holders only._

#### Vital information

* Penguin Dinner starts at 6:30pm at Gold Coast Convention and Exhibition Centre
* You must have a non-zero number next to the Penguin Dinner icon (the middle icon, with the penguin) on your badge to attend
* The Penguin Dinner should conclude around 10pm

#### How do I get there?

Make your way to Foyer C on the ground floor of the Gold Coast Convention and Exhibition Centre.

You will have your badge checked on arrival before entering Arena 2 for the dinner.

---

<a id="pdns"></a>
## Professional Delegates Networking Session (PDNS)

Thursday 16 January, 7pm

_For professional ticket holders and speakers only._

#### Vital information

* PDNS starts at 7pm at QT Surfers Paradise
* Use your travel card to take the light rail to Cypress Avenue (if you do not have a travel card we will have some spare go cards available to give you)
* You must have a 1 next to the PDNS icon (the rightmost icon, with the shaking hands) on your badge to attend
* The PDNS should finish around 9:30pm

#### How do I get there?

At the Broadbeach North station (in front of the convention centre), touch on, and wait on Platform 1 for the next tram to Helensvale.
We will have volunteers at this stop to direct you.

When you get to Cypress Avenue, touch off with your card, and follow the volunteers 150m to the building entrance.

Enter the QT lobby, and you will be directed through to our event area via the red carpet, where you will have your badge checked.

#### Additional info

If you are planning to stay in Surfers Paradise after the event, just be mindful that trams finish around 12am, but busses will continue throughout the night.
