---
layout: page
title: Proposals
permalink: /programme/proposals/
card: call_session_miniconf_date.068f02d9.png
sponsors: true
---

## Important Information

 * Call for Proposals Opens: 24 June 2019
 * Call for Proposals Closes: ~~28 July 2019~~ **Extended to 11 August 2019** [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth)
 * Notifications from the sessions committee: early September 2019
 * Conference Opens: 13 January 2020

## How to submit

**Our call for sessions and miniconfs is now closed.**
Thank you to everyone who submitted a talk, tutorial or miniconf proposal.
Notifications from the sessions committee will be sent out in September 2019.

If you would like to review your proposals, you can continue to do so via your [Dashboard](/dashboard/).

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Go to Dashboard</a>

## About linux.conf.au

linux.conf.au is a conference where people gather to learn about the entire world of Free and Open Source Software, directly from the people who contribute.
Many of these contributors give scheduled presentations, but much interaction occurs in-between and after formal sessions between all attendees.
Our aim is to create a deeply technical conference where we bring together industry leaders and experts on a wide range of subjects.

linux.conf.au welcomes submissions from first-time and seasoned speakers, from all free and open technology communities, and all walks of life.
We respect and encourage diversity at our conference.

## Conference Theme

Our theme for linux.conf.au 2020 is "Who's Watching", focusing on security, privacy and ethics.
As big data and IoT-connected devices become more pervasive, it's no surprise that we're more concerned about privacy and security than ever before.
We've set our sights on how open source could play a role in maximising security and protecting our privacy in times of uncertainty.
With the concept of privacy continuing to blur, open source could be the solution to give us '2020 vision'.

Please let this inspire you, but not restrict you - we will still have many talks about other interesting things in our community.

Deciding what to speak about at linux.conf.au can be a tough challenge, particularly for new speakers.
If you would like some ideas on how to write a talk and how to submit a proposal, we recommend watching E. Dunham's [You Should Speak](https://www.youtube.com/watch?v=3QIQNcGnXes) talk from linux.conf.au 2018.

## Conference Location

linux.conf.au 2020 will be held at the [Gold Coast Convention and Exhibition Centre](https://www.gccec.com.au/).
For more information, please see our [location and venue](/about/location/) guide.

## Proposal Types

We’re accepting submissions for three different types of proposal:

 * Presentation (45 minutes): These are generally presented in lecture format and form the bulk of the available conference slots.
 * Tutorial (100 minutes): These are generally presented in a classroom format. They should be interactive or hands-on in nature. Tutorials are expected to have a specific learning outcome for attendees.
 * Miniconf (full-day): Single-track mini-conferences that run for the duration of a day on either Monday or Tuesday. We provide the room, and you provide the speakers. Together, you can explore a field in Open Source technologies in depth.

## Proposer Recognition

In recognition of the value that presenters and miniconf organisers bring to our conference, once a proposal is accepted, one presenter or organiser per proposal is entitled to:

 * Free registration, which holds all of the benefits of a Professional Delegate Ticket
 * A complimentary ticket to the Speakers' Dinner
 * Optionally, recognition as a Fairy Penguin Sponsor, available at 50% off the advertised price

If your proposal includes more than one presenter or organiser, these additional people will be entitled to:

 * Professional or hobbyist registration at the Early Bird rate, regardless of whether the Early Bird rate is generally available

**Important Note for miniconf organisers**: These discounts apply to the organisers only.
All participants in your miniconf must arrange or purchase tickets for themselves via the regular ticket sales process or they may not be able to attend!

As a volunteer-run non-profit conference, linux.conf.au does not pay speakers to present at the conference; but you may be eligible for financial assistance.

## Financial Assistance

linux.conf.au is able to provide limited financial assistance for some speakers.

Financial assistance may be provided to cover expenses that might otherwise prohibit a speaker from attending such as:

 * Cost of flights
 * Accommodation
 * Other accessibility related costs

To be considered for assistance you can indicate this when making your proposal.
We will try to accommodate as many requests for assistance as possible within our limited budget.

## Accessibility

linux.conf.au aims to be accommodating to everyone who wants to attend or present at the conference.
We recognise that some people face accessibility challenges.
If you have special accessibility requirements, you can provide that information when submitting your proposal so that we can plan to properly accommodate you.

We recognise that childcare and meeting dietary requirements also fall under the general principle of making it possible for everyone to participate, and will be announcing our offering for these in the near future.
If you have concerns or needs in these areas, or in any area that would impact your ability to participate, please let us when submitting your proposal.

## Code of Conduct

By agreeing to present at or attend the conference you are agreeing to abide by the [terms and conditions](/attend/terms-and-conditions/).
We require all speakers and delegates to have read, understood, and act according to the standards set forth in our [Code of Conduct](/attend/code-of-conduct/).

## Recording

To increase the number of people that can view your presentation, linux.conf.au will record your talk and make it publicly available after the event.
We plan to release recordings of every talk at the conference under a [Creative Commons Attribution-ShareAlike Licence](https://creativecommons.org/licenses/by-sa/4.0/).
When submitting your proposal you may note that you do not wish to have your talk released, although we prefer and encourage all presentations to be recorded.

## Licensing

If the subject of your presentation is software, you must ensure the software has an Open Source Initiative-approved licence at the time of the close of our Call for Sessions.

## FAQ

**What is the difference between private and public summary?**

Private Summary is the portion that will be shown to the review team.
Give some more details of the talk and why it should be chosen.

Public Summary is the portion that will be shown to everyone on the website once the schedule is annouced.
Provide enough details of the talk to whet the appetite.