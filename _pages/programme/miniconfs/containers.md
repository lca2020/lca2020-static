---
layout: page
title: Kubernetes, Containers and Docker Miniconf
permalink: /programme/miniconfs/containers/
sponsors: true
---

<p class="lead">
Organised by Angus Lees and Katie McLaughlin
</p>

## When

Tuesday 14 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="containers-miniconf" day="tuesday" closed="True" %}

## About

Anything related to this area of technology is on topic, including immutable/declarative workflows, other container orchestration systems, and tales of fail.
