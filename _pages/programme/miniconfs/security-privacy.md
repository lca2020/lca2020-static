---
layout: page
title: Security, Identity, and Privacy Miniconf
permalink: /programme/miniconfs/security-privacy/
sponsors: true
---

<p class="lead">
Organised by Ben Dechrai and Erin Zimmer
</p>

## When

Tuesday 14 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="security-miniconf" day="tuesday" closed="True" %}

## About

Never has security been more in the news.
Companies around the world know that they are going to have to prioritise identity and privacy within systems designs and processes, to ensure customers and clients are protected more than ever.
While laws like the GDPR give us guidelines and recommendations on best practices for data collection and storage, you need look no further than the Security, Identity, and Privacy Miniconf to level up your skills with talks and practical advice from professionals and thought leaders.

### Security

Do you work in security? Are you a software developer who focuses on security, or are you a pentester? Whatever you do, we all need to keep security in mind.

Whether you would like to talk about the OWASP Top 10, or demo a security testing tool, or write a virus, or show how you used the HaveIBeenPwned API... whatever it is, we want to hear about it.

### Identity

Do you work in identity? Have you worked on a project recently that required you to manage users across disparate systems in one central spot? Identity is critical to pretty much every application today.

Whether you would like to talk about implementing an Identity as a Service Provider, or you've developed a system for verifying identities online, or you have opinions on how identity management can be made better... whatever it is, we want to hear about it.

### Privacy

Do you work with sensitive data? Do you work hard to protect yourself online? Privacy is the act of controlling what's public about you, not the absence of data about you, and it's of paramount importance for myriad reasons.

Whether you want to talk about managing your privacy and anonymity online, or how your organisation implemented the GDPR guidelines and recommendations, or ways that software companies can implement zero-knowledge... whatever it is, we want to hear about it.
