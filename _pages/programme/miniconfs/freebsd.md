---
layout: page
title: FreeBSD Miniconf
permalink: /programme/miniconfs/freebsd/
sponsors: true
---

<p class="lead">
Organised by Jonathan Eastgate and Deb Goodkin
</p>

## When

Tuesday 14 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="freebsd-miniconf" day="tuesday" closed="True" %}

## About

The FreeBSD Open Source Operating System is one of the oldest, largest, and most successful open source projects, with a long history of innovation.
FreeBSD descended from Berkeley Unix back in the early '90s, with its lineage dating back 50 years to the original UNIX.

In this one day miniconf, speakers will host a series of keynotes, educational sessions, roundtable discussions, best practice conversations and exclusive networking opportunities.

The focus of the miniconf will be to share this information with the Linux community to encourage working together.
While recognizing that FreeBSD is a minority in the Linux world, including their diverse voices in discussions, will help strengthen the open source ecosystem.
