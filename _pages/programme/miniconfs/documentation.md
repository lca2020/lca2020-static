---
layout: page
title: Documentation Miniconf
permalink: /programme/miniconfs/documentation/
sponsors: true
---

<p class="lead">
Organised by Joe Robinson, Dan Macpherson, Brian Moss and Lana Brindley
</p>

## When

Monday 13 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="docs-miniconf" day="monday" closed="True" %}

## About

### Introduction

Documentation brings value - we can break this down into change management, reduced silos, increased communication, more positive customer relationships, and education.
This miniconference demonstrates the ethical imperative to support and expand documentation services within a business.
Without such a documentation service, change, silos, customer relationships, and education atrophy and attenuate within a business to the detriment of all.

This documentation miniconference is open to professionals working in Australia, with invitations extended to international professionals.
Following the LCA2020 theme, we are interested in the ethical impact documentation incites.
We want to spotlight the ethical impact Documentation experts provide when they manage change, information silos and knowledge bases, customer relationships, Design for Users, and education.

We want to hear your stories, information, research, and ideas on documentation and ethics.

Whether you are are a professional writer, a project contributor, or just interested in the art of communication, we invite you to come along and discover the absolute value of documentation.

### Presentation topics

We are accepting presentations on the following topics, and others related to documentation not listed here:

* Business ethos and technical documentation
* Technical documentation in the future
* The best practices in your  writing
* Documentation as code
* Embedded documentation
* What happens without the docs?
* User Experience
* Translation and internationalization
* Development and publishing infrastructure
* Working with engineers and other teams
* Coders who write, writers who code
* Information Architecture
* Writing for diverse audiences
* Open Source documentation and writing in online communities
* Package Design
* User Interfaces
* Writing Style
* Processes and methodologies
* Editing
* Tips, tricks, and tales from field
* Presentation Formats
* Knowledge Base Design
