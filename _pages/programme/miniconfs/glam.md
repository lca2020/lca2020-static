---
layout: page
title: GO GLAM Miniconf
permalink: /programme/miniconfs/glam/
sponsors: true
---

<p class="lead">
Organised by Sae Ra Germaine and Hugh Rundle
</p>

## When

Tuesday 14 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="glam-miniconf" day="tuesday" closed="True" %}

## About

GO GLAM (Generous and Open: Galleries, Libraries, Archives, Museums) has emerged from the LCA2018 Open GLAM mini-conference.
As GLAM institutions are becoming more and more technology-dependent there is an increasing imperative for the community maintaining these cultural institutions to have a sophisticated understanding of the opportunities, challenges and risks of digital technologies now and in the foreseeable future.
Academic libraries in particular are taking a specific interest in the concept of Open and are great advocates for the F.A.I.R. (Findable, Accessible, Interoperable, Reusable) principles which are very tightly linked to the Open Source and Open Technology values.

Many public GLAM institutions have, like other government agencies, embraced free and open-by-default licensing for documents and data they hold in the form of Creative Commons copyright licenses.
Using these licences recognises that the cultural collections held by our Galleries, Libraries, Archives and Museums are a public good and access should be maximised.
The National Library has discovered the power of crowdsourcing via Trove, particularly with its collection of digitised historical newspapers.
The annual GovHack competition encourages use of this public data.
Some libraries are even releasing workshops and training material for free.

Yet all these efforts have, for the most part, not significantly changed the way the vast majority of users interact with collections.
How do we prime users to engage critically with notions of access and become members of our Open community?
How does the modern library, archive, or museum provide open access without increasing the digital divide?
How best can GLAM institutions support and embrace more meaningful community interaction with their collections, whilst continuing to fulfil their responsibilities as custodians of cultural works?
How does and should the FOSS community engage, encourage and develop with the GLAM community?

As everything becomes more open and more connected, GLAM institutions must simultaneously increase focus on new data security, privacy, and control issues: everything from protecting user data against new threats, to developing protocols for digital access to sacred Indigenous cultural knowledge.
What can GLAM workers and technologists learn from each other when it comes to the ethics and practice of privacy, openness and custodianship?

What, that is, does Generous and Open GLAM look like?

We are bringing together some key contributors to this space to raise awareness of gaps that GLAM institutions are recognising, and discussing some possible paths forward.
