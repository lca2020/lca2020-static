---
layout: page
title: Creative Arts Miniconf
permalink: /programme/miniconfs/creative-arts/
sponsors: true
---

<p class="lead">
Organised by Jonathan Woithe
</p>

## When

Monday 13 January 2020

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="creative-arts-miniconf" day="monday" closed="True" %}

## About

The creative arts spans a wide range of endeavours such as music, video, dynamic and static art installations, podcasting, film, and even robotics.
Over recent decades, free and open source software (FOSS) and open hardware have transformed the creative arts, making hardware and software resources accessible to all creative artists regardless of their background.
This in turn has allowed them to express themselves through their art with far fewer restrictions than would otherwise be the case.
Importantly, having full control of hardware and software ensures that artists truly own their works: their ability to practise and access their art cannot be taken away on the whim of a company.
Artists can control their own creative output.

The LCA2020 Creative Arts Miniconf will provide a place where creatives can share their workflows, tools and art with the broader open software community.
The miniconf will welcome presentations and attendance by both users and developers to promote a free exchange of ideas and information.
Presentations may be on any topic which relates to the use of open source software or open hardware in a creative arts context.
Above all, the miniconf aims to inspire attendees by highlighting the many and varied ways that FOSS and open hardware empowers creatives to express themselves.
