---
layout: page
title: Accommodation
permalink: /attend/accommodation/
sponsors: true
---

There are many hotels offering rooms for a range of budgets on the Gold Coast.
Please note that the conference occurs during school holidays, which may lead to increased rates at some locations.

## Discounts at local hotels

linux.conf.au 2020 has also arranged discounted accommodation with surrounding providers.
Full details are available to delegates who have registered for an account on the website.

These rooms are subject to availability and are subject to change.

<div class="my-3">
  <a class="btn btn-primary btn-md" href="/pages/accommodation/">Discount codes and instructions</a>
</div>
