---
layout: page
title: Financial Assistance
permalink: /attend/assistance/
sponsors: true
card: financial_assistance.796d004e.png
---

Thanks to the support of our Outreach and Inclusion sponsor Google, we have budget set aside for providing financial assistance to linux.conf.au attendees who might otherwise find it difficult to attend.
This program is a key part of our outreach and inclusion efforts for linux.conf.au, and is in addition to the financial assistance offered to main conference speakers.

Financial assistance can cover several types of expenses related to attending linux.conf.au.
You may apply for full or partial financial assistance from one or more of the following areas:

* A ticket to the conference (at the hobbyist or student level)
* Flights or other transport to the Gold Coast
* Accommodation costs
* Other costs associated with attending such as a support worker, child care or a substitute teacher.

Please ask only for what you need in order to be able to attend the conference and highlight any opportunities you would use to save costs, such as finding cheaper accommodation further from the conference venue (public transport, such as the G:Link light rail, provides easy access to the venue).

## Application

To apply for financial aid please fill out the application form.
Applications will be approved progressively on a weekly basis, so please apply early to have a greater chance for funding.
Once the budget allocation has been exhausted, the application process will be closed.

<div class="my-3">
  <a class="btn btn-primary btn-md" href="https://forms.gle/EwDekE5hDCyn9s1h9" target="_blank">Apply Now</a>
</div>

## Selection criteria

In addition to allocating financial aid on the basis of need, we also take into account how we can get the most benefit for all linux.conf.au attendees and the wider community.

In your application please mention if you:

* Would be helping linux.conf.au achieve our goal of bringing together of a diverse set of backgrounds and perspectives
* Are a volunteer or helping in another way to make the conference more awesome
* Are a miniconf speaker (or have applied to be one)
* Are a member or organiser of an open source or related group and would share your linux.conf.au experience with the wider community
* Are a teacher or other educator and attending linux.conf.au would benefit your students as well
* Are a maintainer or contributor to an open source project

Note that these are considered as factors, but they are not a checklist.
You do not need to have any of the above to be eligible for financial aid.

## Reimbursement process

If granted assistance, you will still be expected to organise and purchase travel and accommodation yourself and await reimbursement.
You will be given a maximum amount of reimbursement based on the assistance you requested.
Once assistance is granted, you can start making bookings and sending us your receipts.

If you have any questions about financial assistance please contact us at [assistance@lca2020.linux.org.au](mailto:assistance@lca2020.linux.org.au).
