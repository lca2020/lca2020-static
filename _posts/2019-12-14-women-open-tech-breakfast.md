---
layout: post
title: Women of Open Tech Working Breakfast
card: women_tech_breakfast.c438024d.png
---

<p class="lead">With a record number of women speaking at LCA2020, we are hosting a Women of Open Technology Breakfast on Tuesday at 7am.</p>

The breakfast gives women an opportunity to have a ‘meta discussion’ about diversity and the role women play in the LCA community.
Attendees will also explore what our community could do to become a more inclusive place for women and other marginalised groups.

## Want to join the discussion?

While the breakfast is free, tickets are limited to 50 seats.
You can register for a ticket today via the [registration dashboard](/tickets/category/8).

The breakfast primarily invites delegates from groups that are under-represented at the conference, including women, people from the LGBTQIA+ community, people with an impairment, and people of colour.
If you are not a member of an under-represented group, you are welcome to come at the invitation of someone who is.
This event has a strictly limited number of tickets available.
