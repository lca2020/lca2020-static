---
layout: post
title: Statement on Bushfires
---

<p class="lead">With linux.conf.au 2020 rapidly approaching, many delegates are looking to their travel and there have been some concerns surrounding the fires in Australia.</p>

The impact of the bushfires across Australia have been devastating for many Australians as well as our native animals and the environment.
Due to the impact they have had, our charity raffle will be in aid of causes supporting those impacted by these horrific fires.

Currently the main fire-affected areas are in New South Wales, Victoria, and South Australia.
Last year the Gold Coast Hinterland was affected by bushfires, however this has long since been extinguished.

The area of the Gold Coast where the conference is being held is directly on the coastline and has not been impacted by the fires.
We do not believe there will be any risk of bushfires impacting the conference and the venues it is using.

There has also been a lot of attention on smoke from the fires impacting Australia.
This is primarily affecting areas in the south-east of Australia, including Sydney, Canberra, and down into Victoria.
The Gold Coast is not currently affected by smoke and the air quality is the same as usual, and we expect this to continue through the week of the conference.

linux.conf.au 2020 will proceed as planned and we are looking forward to seeing you all on the Gold Coast very soon.

If you have any questions feel free to reach out via [email](mailto:contact@lca2020.linux.org.au).
