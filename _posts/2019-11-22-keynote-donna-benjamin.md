---
layout: post
title: "Keynote announcement: Donna Benjamin"
card: keynote_donna.5959e06c.png
---

<p class="lead">We are excited to announce Donna Benjamin as our first keynote speaker for LCA2020.</p>

<img class="img-fluid float-sm-right profile-pic" src="/media/img/donna_benjamin.jpg" width="250">

Donna is a big advocate for everything Open Source as she believes it gives people the freedom to design their futures.

Donna’s keynote will touch on our theme for 2020 ‘Who’s watching’ and pose a couple of counter questions.
More than who is watching, Donna’s keynote will leave you asking yourself ‘why are they watching?’, ‘why does it matter to me?’ and ‘why does it matter to us?’.

## A bit about Donna

Donna Benjamin is a passionate advocate of Free and Open Source Software. She has served on a number of community boards and committees, organised events (including this one) and made various contributions to various projects over many years. In 2013, Donna received the ‘Rusty Wrench’ award for service to the Australian and New Zealand Free and Open Source Software community. Currently, she's employed by Red Hat as an Engagement Lead in the Open Innovation Labs, but also runs her own business, Creative Contingencies.

## Interested?

If you’re interested to hear Donna speak make sure you register for linux.conf.au 2020.
You can purchase your [tickets](/attend/tickets/) today.
