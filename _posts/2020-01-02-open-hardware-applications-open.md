---
layout: post
title: Open Hardware Miniconf Applications Open
---

<p class="lead">Apply now for the Open Hardware MiniConf to build a Perception Module for machine-learning and a self-driving car.</p>

The Open Hardware MiniConf is a 1-day event at linux.conf.au, which provides a fun introduction to electronics and hardware hacking.
It starts with an assembly session, where you will have the opportunity to learn how to solder while building your own “Perception Module” with a machine-learning vision system, and attach it to an R/C car chassis to use it as the brains of a self-driving car.

## What is the project?

This project is a further development of the Donkey Car project that was run last year, but with the control system split into a more general purpose Perception Module that you can use for many other purposes.
The Perception Module has Lego-compatible holes so that it can be clipped onto a car chassis or built into your own project.

Putting the module on a car chassis is a fun way to get started, but you can also use it in totally different applications.
Use machine learning to teach it how to recognise your pets, or sort Lego, or detect weeds in the garden, or for gesture control.

## How do I attend?

Places for the assembly session are strictly limited, so registration is required.
Even if you ticked the “Open Hardware MiniConf” box on the LCA registration form, you still need to apply for the miniconf separately if you wish to participate in the assembly session.

The cost of the assembly session is AUS$140, which includes all the parts required for you to build your own Perception Module and fit it onto a converted R/C car chassis to make a self-driving model car.
Some subsidised places are also available for those who couldn’t otherwise afford to attend.
The actual value of the parts provided is approximately AUS$260, but thanks to a generous benefactor the cost has been kept low in order to allow more people to participate.

## How can I register?

The registration process for the Open Hardware MiniConf is different this year.

Instead of a direct registration form, we’re using a 2-step process.
This will help us avoid the registration rush that usually has the event sold out within a few hours.

This will also allow us to do a better job of balancing out the allocation of different types of tickets, including normal tickets, supporter tickets, and subsidised tickets, to encourage greater diversity. The result should be more fair than a simple race to the rego form like in previous years.

So take a deep breath, and relax. It’s not a race this year!

**Step 1:** Submit the application form using the link below.
We’ll collect applications and then assign spots according to the different types of tickets.

**Step 2:** We’ll follow up this coming weekend to let you know if you have a ticket assigned.
If you do, you’ll receive details of how to submit your payment and receive your ticket.

Please note that the application form includes a question about whether you identify as belonging to an under-represented group.
The reason for this is to allow us to prioritise tickets for people who would otherwise not be well represented at the event, to improve the overall diversity.

Of course, this is a very difficult thing to define, and you may be wondering if this applies to you.
We don’t want to be in a position of making a judgement call, so we’re leaving it up to your discretion.

Because the lead organisers of the OHMC are stereotypical straight white middle-aged males, we’re very aware that we don’t necessarily know the best way to encourage and support the needs of under-represented people.
We just want to make this event as inclusive and friendly as possible. If you have any feedback for us about what we could do better, please let us know.

See the [Open Hardware Miniconf website](http://www.openhardwareconf.org/wiki/OHMC2020) for more information.

To register for the assembly session, fill out the [application form](https://forms.gle/2ZQv744b6ny7mXqh8).

-- Jon, Andy and the OHMC2020 team