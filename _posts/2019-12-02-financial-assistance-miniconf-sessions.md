---
layout: post
title: Financial Assistance now available
card: financial_assistance.796d004e.png
---

<p class="lead">Financial assistance applications are now open, and miniconf session proposals are closing soon.</p>

There are only six weeks until linux.conf.au!
We have a couple of things that we wanted to let you know about in the lead up to the conference.

## Financial Assistance

Thanks to the support of our Outreach and Inclusion sponsor Google, we have budget set aside for providing financial assistance to linux.conf.au attendees who might otherwise find it difficult to attend.
This program is a key part of our outreach and inclusion efforts for linux.conf.au, and is in addition to the financial assistance offered to main conference speakers.

Anyone can apply for financial assistance for linux.conf.au 2020.
The assistance can cover things such as a ticket to the conference, travel, accommodation, or other costs that you would incur to attend.
We will be processing applications on a weekly basis until funds are exhausted, so we encourage you to apply early for a greater chance of being accepted.

Full details about the program and the application form can be found in our [Financial Assistance Guide](/attend/assistance/).

## Miniconf Call for Sessions

There is still time to submit a proposal to one of our [miniconfs](/programme/miniconfs/).
As a reminder, we have 12 miniconfs that you could speak at:

* [System Administration](/programme/miniconfs/system-administration/)
* [Creative Arts](/programme/miniconfs/creative-arts/)
* [Games and FOSS](/programme/miniconfs/games/)
* [Open ISA (RISC-V, OpenPOWER, etc)](/programme/miniconfs/open-isa/)
* [The Absolute Value of Documentation](/programme/miniconfs/documentation/)
* [Open Education](/programme/miniconfs/open-education/)
* [Kernel](/programme/miniconfs/kernel/)
* [GO GLAM (Generous and Open: Galleries, Libraries, Archives, Museums)](/programme/miniconfs/glam/)
* [Security, Identity and Privacy](/programme/miniconfs/security-privacy/)
* [Kubernetes, Containers and Docker](/programme/miniconfs/containers/)
* [FreeBSD](/programme/miniconfs/freebsd/)
* [Open Hardware](/programme/miniconfs/open-hardware/)

If you have something to talk about in any of these areas, please consider submitting a proposal to one or more miniconfs.
The submission portal will be **closing soon**, so get in today to ensure you do not miss out.
Further information about each [miniconf](/programme/miniconfs/) can be found on their respective pages.


If you have any questions about linux.conf.au 2020, you can contact us via [email](mailto:contact@lca2020.linux.org.au).
