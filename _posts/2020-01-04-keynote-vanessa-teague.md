---
layout: post
title: "Keynote announcement: Vanessa Teague"
card: keynote_vanessa.8d670b4a.png
---

<p class="lead">Our fourth keynote speaker for LCA2020 is Dr Vanessa Teague.</p>

<img class="img-fluid float-sm-right profile-pic" src="/media/img/vanessa_teague.jpg" width="250">

Dr Vanessa Teague is a cryptographer with longstanding interests in elections and privacy.
She divides her time between designing new protocols and explaining why other protocols are not secure.
Vanessa is a big advocate for Open Source, especially in the case of elections where it allows people to find and fix bugs and to share an honest assessment of a system’s strengths and weaknesses.

Vanessa’s keynote will explore what good software could do for democracy and why it is mostly doing the opposite.
She will also discuss what we can do to fix these problems.
Vanessa will start by exploring the role of software in elections before speaking more specifically about what we could build to improve the situation.

## A bit about Vanessa

Dr Vanessa Teague is a cryptographer with longstanding interests in elections and privacy.
She divides her time between designing new protocols and explaining why other protocols are not secure.
Recently she has worked on cryptographic examinations of voting systems in Switzerland and Australia, election audits in California, and data re-identification in Australia.

## Interested?

If you’re interested to hear Vanessa speak make sure you register for linux.conf.au 2020.
You can purchase your [tickets](/attend/tickets/) today.
