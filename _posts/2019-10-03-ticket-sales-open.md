---
layout: post
title: Ticket Sales Open
card: ticket_sales_open.14db84a8.png
---

<p class="lead">We are pleased to announce that ticket sales for linux.conf.au 2020 have now launched!</p>

Early bird tickets are available now until the end of October 2019 or when the allocation is exhausted, whichever comes first.
Prices and inclusions are available on the [tickets](/attend/tickets/) page.
Buy your ticket now via your [dashboard](/dashboard/).

## Children

We will once again have childcare services available for children at the conference, at no cost to parents.
For younger children, there will be a kids club and childminding service throughout the week on-site at the Gold Coast Convention and Exhibition Centre.
We are looking to run a code club for older children, with details still being confirmed.
If you would like to take advantage of this, please include details on your attendee form during the registration process.

## Accommodation

We have secured discount codes for a range of local hotels to help make accommodation more affordable.
As the conference is on during the school holidays, we recommend booking accommodation soon.
See the [accommodation](/attend/accommodation/) page for details.

## Volunteering

If you are looking to volunteer at the conference you can apply to be a volunteer today.
As linux.conf.au is a community-focused, grassroots conference, it requires the support of volunteers to make the week possible.
Please see the [volunteers](/attend/volunteer/) page for full details.


If you have any questions about ticket sales or anything to do with linux.conf.au 2020, you can contact us via [email](mailto:contact@lca2020.linux.org.au).
