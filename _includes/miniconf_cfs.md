{% if include.closed %}
  Submissions to this miniconf are now closed.

  <a class="btn btn-md btn-outline-primary" href="/schedule/#{{ include.day }}">View the schedule</a>
{% else %}
  {{ include.lead | default: "The call for sessions is now open!" }}

  * Call for Sessions Opens: 3 October 2019
  * Call for Sessions Closes: ~~8 December 2019~~ **Extended to 22 December 2019** [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth)
  * Early Acceptance: From 17 November 2019

  We understand that some people might need to know their proposal acceptance status earlier than December, in order for them to arrange time off, travel, etc.
  To assist with this, there will be an early acceptance window starting on 17 November 2019.
  If you require notice during this early acceptance window, please indicate this in the Private Abstract when filling out your proposals.

  {% if include.miniconf_slug %}
  <a class="btn btn-md btn-outline-primary" href="/proposals/submit/{{ include.miniconf_slug }}/">Submit a proposal</a>
  {% endif %}
{% endif %}
